// Arrays
const sequence = [1, 2, 3]
sequence.push(5) // append to the end
console.log(sequence)
sequence.pop(); // removes last item
console.log(sequence)
sequence.unshift(6) // appends to the beginning
console.log(sequence)
sequence.shift() // removes first item
console.log(sequence)

for (let i = 0; i < sequence.length; i++) {
    console.log(sequence[i])
}

for (let number of sequence) {
    console.log(number)
}

console.log(sequence.concat([5, 6, 7]))
console.log(sequence.slice())




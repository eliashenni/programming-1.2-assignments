let i = 7; // number (double)
i = 8; // number (double)
i = 7 + i;
i += 7;
console.log('i - ' + i);

let s = "Hello World"; // String (Single or double quotes)
console.log('s = ' + s);

s = true; // true/false
console.log('s = ' + s);
let c = 'C'; // no Char

const con = 'elias'; // final that you cant change

// Global scope
var v = 'bruh'; // Use let and const instead
v = 7;

{
    let l = 7;
    var v2 = 9;
    console.log('l = ' + l)
}
// console.log('l = ' + l) Not allowed
console.log('v2 = ' + v2) // Works because it is globally scoped

let v3 = undefined;
let v4 = null;
console.log(v3);
console.log(v4);

// == is not that strict
if (v3 === undefined) {
    console.log('undef!');
}
if (v3 === null) {
    console.log('null!');
}
if (v3 === 0) {
    console.log('zero!');
}
if (v3) {
    console.log('Has Something');
}
if ('' === false) {
    console.log('Yes, they are equal!');
} else {
    console.log('No!');
}


console.log("7" + 2) //Turns the 2 into a string
console.log("7" - 2) //Turns 7 into number
// (If String is not number it will result in NaN(Not a Number))


// Templating
const name = 'Eli';
const myString = `Hello ${name}`; // '' "" Kinda like String.format
console.log(myString);

//  / * + - ++ -- post and prefix notations ! == === > <= PRECEDENCE : ( )

// Type of
const theType = typeof 'h';
console.log(theType);
console.log(theType.charAt(0));

// Infinity
console.log(-1 * Infinity);

// Loops and ifs
let a = 7
if (a === 89) {
    console.log();
} else if (false) {

} else {

}

while (a--) {
    console.log(a);
}

do {

} while (false);

for (let i = 0; i < 10; i++) {
    console.log(i + 1)
}

const n = 'Eli';
switch (n) {
    case 'Eli':
        console.log('Its Eli');
        break;
    case 'Jan':
        console.log('Its Jan');
        break;
    default:
        console.log(`${n} not supported`)
}

console.log('Root: ' + Math.sqrt(144)); //abs cos sinus round

// Only works in browser (Not supported on node.js)
// const username = prompt('Please enter your name')








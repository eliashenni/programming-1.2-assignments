"use strict";
console.log(square(12))
console.log(box(12))
console.log(square('Hello'))
console.log(square())
doesSomething(7, 7, 8, 8)
const c = doesSomething(7)
console.log('c = ' + c)

const f3 = square;
console.log(f3(10))

function square(number) {
    return number * number
}

// Normal return function
function box(number) {
    return number * number * number
}
// lambda way
const cube = x => x*x*x


function doesSomething(someNumber, someString) {
    console.log('someString: ' + someString)
}

console.log(square(5))

function power(base, exponent = 1) {
    // let result = 1
    // for (let i = 0; i < exponent; i++) {
    //     result *= base
    // }
    //if (exponent === undefined) exponent = 1

    return (exponent === 0) ? 1 : power(base, exponent - 1) * base
    // if (exponent === 0) {
    //     return 1
    // }
    // return power(base, exponent - 1) * base

}
console.log(power(5,4))

let thing
thing = 5
console.log(thing)

"use strict"

console.log(power(5))
console.log(power(6, 2, 'This gets thrown away'))










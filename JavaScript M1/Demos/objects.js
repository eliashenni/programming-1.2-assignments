// Day1 is a constant
const day1 = {
    squirrel: false,
    events: ['work', 'touched tree', 'pizza', 'running']
}
// Objects of day1
console.log(day1.squirrel)
console.log(day1.events)

// Adding new object to day1
day1.weather = 'sunny'
console.log(day1)

// Nested Object
day1.date = {
    day: 19,
    month: 'April',
    year: 2021
}
day1.date.weekday = 'monday'

day1.headlines = ['Fire in Anderlecht', 'Anderlecht joins Euro Competition']
console.log(day1.headlines)
console.log(day1['headlines'])
day1['2'] = 'two'

console.log(day1)
console.log(day1[2])

for (let key of Object.keys(day1)) {
    console.log(key)
}



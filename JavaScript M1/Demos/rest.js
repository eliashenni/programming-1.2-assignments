// Rest Operator
function printAll (array) {
    for (let arrayElement of array) {
        console.log(arrayElement)
    }
}

function printRest (...array) {
    for (let arrayElement of array) {
        console.log(arrayElement)
    }
}

printAll(['red', 'green', 'blue'])
console.log('------')
printRest('red', 'green', 'blue')

let colors = ['red', 'green', 'blue']
let secondary = ['violet', 'yellow', 'cyan']
// colors.push(secondary)
// for (let string of secondary) {
//     colors.push(string)
// }

console.log(colors)
// Rest and spread... to take apart array
// Spread operator
colors.push(...secondary)
console.log(colors)

let today = {
    day: 19,
    month: 'April',
    year: 2021
}

const day1 = {
    squirrel: false,
    events: ['work', 'touched tree', 'pizza', 'running'],
    ...today
}
console.log(day1)

// day1.date = today










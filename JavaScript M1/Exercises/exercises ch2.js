// ex1 Looping a triangle ------------
let d = ''
for (let i = 0; i < 7; i++){
    d = ''
    for (let j = 0; j <= i; j++) {
        d += '#'
    }
    console.log(d)
}

// OR
let ch = ""
while(ch.length <= 7) {
    console.log(ch += "#")
}

// ex2 Fizzbuzz -------------
for (let i = 1; i <= 100; i++) {
    if ((i % 3 === 0) && (i % 5 === 0)) {
        console.log('FizzBuzz')
    }
    else if ((i % 3 === 0)) {
        console.log('Fizz')
    }
    else if (i % 5 === 0) {
        console.log('Buzz')
    }
    else {
        console.log(i)
    }
}

// OR
for (let i = 1; i <= 100 ; i++) {
    console.log(i % 3 === 0 ? (i % 5 === 0 ? "FizzBuzz " : "Fizz") : (i % 5 === 0 ? "Buzz" : i));
}

// ex3 Chessboard ----------------
let size = 8
let board = ''
for (let i = 0; i < size; i++) {
    if (i % 2 === 0) {
        for (let j = 0; j < size; j++) {
            board += ' #'
        }
    } else {
        for (let j = 0; j < size; j++) {
            board += '# '
        }
    }
    board += '\n'
}
console.log(board)
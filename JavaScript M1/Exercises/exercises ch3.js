// ex1 Minimum
console.log('---Exercise 1 Minimum---')
function min(number1, number2) {
    return (number1 > number2) ? number2 : number1
}

console.log(min(0, 10))
console.log(min(0, -10))

// ex2 Recursion
console.log('---Exercise 2 Recursion---')
function isEven(number) {
    if (number === 0) return true
    if (number === 1) return false
    if (number < 0) return isEven(-number);
    return isEven(number - 2)
}

console.log(isEven(50))
console.log(isEven(75))
console.log(isEven(-1))

// ex3 Bean Counting
console.log('---Exercise 3 Bean Counting---')
function countBs(text) {
    let count = 0
    for (let i = 0; i < text.length; i++) {
        if (text[i] === 'B') {
            count++
        }
    }
    return count
}

function countChar(text, letter) {
    let count = 0
    for (let i = 0; i < text.length; i++) {
        if (text[i] === letter) {
            count++
        }
    }
    return count
}

console.log(countBs('BBC'))
console.log(countChar("kakkerlak", "k"))




// ex1 The Sum of a Range
console.log('---Exercise 1 The Sum of a Range---')

function range(start, end, step = 1) {
    const result = []
    if (end > start) {
        for (let i = start; i <= end; i += step) {
            result.push(i)
        }
    } else {
        if (step === 1) step = -1
        for (let i = start; i >= end; i -= -step) {
            result.push(i)
        }
    }
    return result
}

function sum(array) {
    let sum = 0
    for (let i = 0; i < array.length; i++) {
        sum += array[i]
    }
    return sum
}

console.log(sum(range(1, 10)))
console.log(sum(range(1, 10, 2)))
console.log(sum(range(5, 2, -1)))

// ex2 Reversing an array
console.log('---Exercise 2 Reversing an array---')

function reverseArray(array) {
    let result = []
    for (let i = array.length - 1; i >= 0; i--) {
        result.push(array[i])
    }
    return result
}

function reverseArrayInPlace(array) {
    for (let i = 0; i < Math.floor(array.length / 2); i++) {
        let first = array[i]
        array[i] = array[array.length - 1 - i]
        array[array.length - 1 - i] = first
    }
    return array
}

console.log(reverseArray(["A", "B", "C"]))
let arrayValue = [1, 2, 3, 4, 5];
reverseArrayInPlace(arrayValue);
console.log(arrayValue);

// ex3 A List
console.log('---Exercise 3 A List---')

function arrayToList(array) {
    let list = null
    for (let i = array.length - 1; i >= 0; i--) {
        list = {value: array[i], rest: list}
    }
    return list
}
console.log(arrayToList([10, 20]));
// console.log(listToArray(arrayToList([10, 20, 30])));
// console.log(prepend(10, prepend(20, null)));
// console.log(nth(arrayToList([10, 20, 30]), 1));








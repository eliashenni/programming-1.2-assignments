function printObject(o) {
    console.log(`Object: ${o}`)
}

const obj = {
    name: 'Eli',
    age: 20
}
printObject(obj)

const name = obj.name
const age = obj.age
console.log(`Name: ${name}, Age: ${age}`)

// const {name, age} = obj

const arr = [45, 92, 12]
console.log(`First Element: ${arr[0]}, Second element: ${arr[1]}`)
const [first, second] = arr
console.log(`First Element: ${first}, Second element: ${second}`)

f2(arr)

function f2([first, second]) {
    console.log(`First element: ${first}, Second element: ${second}`)
}

const person = {
    name: 'Eli',
    age: 20,
    happy: true,
    course: {
        courseName: "Programming 1.2"
    }

}
//<person> <name>Eli</name> <age>20</age> </person>  XML
// { "name": "Eli", "age": 20 }                      JSON

console.log(JSON.stringify(person))
console.log(JSON.stringify([1,2,3]))
console.log(JSON.stringify({id: 7, children: [1, {name: "Jack"}]}))

const o1 = JSON.parse('{"test": false, "array": ["3", 4, {"a": true}]}')
console.log(o1.array[1])
console.log(o1.array[2].a)



function f1() {
    console.log()
}

const f2 = () => {
    console.log('Hello')
}

const f3 = function () {
    console.log()
}

// Higher-order function
function handleAll(array, someFunction /*f*/) {
    for (let i = 0; i < array.length; i++) {
        const elementU = array[i]
        someFunction(elementU) // f is called with ONE arg
    }
}

const a = [1,2,3]
function f(n) {
    console.log("Number: " + n )
}
handleAll(a, f) // dont use f() because we want to pass
                // the function not pass it

const x = 7
const b = ['ABC', "test", `X=${x}`]
b.forEach(f4)
function f4(string, index, entireArray) {
    console.log('STRING: ' + string + ' (index ' + index + ')')
}

// b.forEach(console.error)

b.forEach(element => console.log('element: ' + element))

// Predicate
const filteredArray = b.filter((element, index) => {
    // return element === element.toUpperCase()
    // return index % 2 !== 0
    return element.charAt(0) === 't'
})

console.log(filteredArray)

// MAP --> maps elements (not to be confused with the 'Map' data type)
const mappedArray = b.map(element => element.toLowerCase())
console.log(mappedArray)

const numbers = [1.2, 7.5, 63.9]
console.log(numbers.map(n => Math.round(n)))

// Reduce --> reduce an array into a single value
const singleValue = b.reduce((previous, current) => {
    return previous + " --- " + current;
}, "Start string")
console.log(singleValue)

// First, my lambda will be called with element ZERO and element ONE
// Next, my lambda will be called with the VALUE so far and element TWO

const theSum = numbers.reduce((acc, value) => {
    return acc + value
})
console.log('theSum: ' + theSum)
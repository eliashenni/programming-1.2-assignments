class Computer {
    static RAM_UNIT = 'GB';
    constructor(cpuSpeed, amountOfRam) { // NOT 'Computer'
        this._amountOfRam = amountOfRam;
        this._cpuSpeed = cpuSpeed;
    }

    get cpuSpeed() {
        return this._cpuSpeed
    }

    set cpuSpeed(cpuSpeed) {
        if (cpuSpeed < 0) {
            throw new Error("Has to be greater then 0")
        }
        this._cpuSpeed = cpuSpeed;
    }

    fancyPrint() {
        console.log("COMPUTER")
        console.log("========")
        this.print()
    }

    print() { // NOT function
        console.log(`CPU: ${this._cpuSpeed} RAM: ${this._amountOfRam}${Computer.RAM_UNIT}`)
    }
}

const c1 = new Computer(3.0, 8)

c1.fancyPrint()
c1.cpuSpeed = 2.5 // CALLING A SETTER!
c1.fancyPrint()
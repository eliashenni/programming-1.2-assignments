// Exercise 3 --- Everything
// function every(array, test) {
//     let a = true
//     array.forEach(element => {
//         if (!test(element)) {
//             a = false
//         }
//     })
//     return a
// }

function every(array, test) {
    return array.some(test)
}

console.log(every([1, 3, 5], n => n < 10));
console.log(every([2, 4, 16], n => n < 10));
console.log(every([], n => n < 10));
package be.kdg.prog12.collections;

import java.util.*;

public class Names {
    public static void main(String[] args) {
        List <String> names = new LinkedList<>();
        names.add("Albert");
        names.add("Henry");
        names.add("Josephine");
        names.add("Annabelle");
        names.add("Ashraf");

        System.out.println(names.get(0));
        System.out.println(names.get(names.size() - 1));
        System.out.println();

        for (String name : names) {
            System.out.println(name);
        }
        System.out.println();

        System.out.println("The List " + (names.contains("Georgie")? "does " : "does not ") + "contain Georgie");

        for (int i = 0; i < names.size(); i++) {
            if (names.get(i).startsWith("A")) {
                names.remove(i);
                i--;
            }
        }
        System.out.println();
        System.out.println(names);

        //names.removeIf(o -> o.charAt(0) == 'A');

        System.out.println("\n Going to iterate now: \n");
        Iterator<String> it = names.iterator(); // Positioned BEFORE the first element
        while (it.hasNext()) { // Do you have a next element?
            String name = it.next(); // TWO things: move ahead AND return element
            if (name.startsWith("A")) {
                it.remove();
            }
        }

        for (Iterator <String> it2 = names.iterator(); it2.hasNext(); /*blank*/) {
            String name = it2.next();

        }

        Collection<String> col = new ArrayList<>();
    }
}

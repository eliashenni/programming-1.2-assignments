package listEvenNumbers;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ListEvenNumbers {
    public static void main(String[] args) {
        List<Integer> numberList = new LinkedList<>();
        Random random = new Random();

        for (int i = 0; i < 20; i++) {
            numberList.add(i, random.nextInt(50) + 1);
        }

        System.out.println(numberList);

        int[] numberArray = new int[20];
        for (int i = 0; i < numberList.size(); i++) {
            numberArray[i] = numberList.get(i);
        }

        for (int num : numberArray) {
            System.out.print(num + " ");
        }

        List <Integer> evenNumbers = new LinkedList <> ();
        for (int i = 0; i < numberArray.length; i++) {
            evenNumbers.add(i, numberArray[i]);
        }

        for (int i = 0; i < evenNumbers.size(); i++) {
            if (evenNumbers.get(i) % 2 == 1) {
                evenNumbers.remove(i);
                i--;
            }
        }

        System.out.print("\nAll odds gone: " + evenNumbers);

    }
}

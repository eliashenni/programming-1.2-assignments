package be.kdg.prog12.collections;

import be.kdg.prog12.collections.bar.Drink;
import be.kdg.prog12.collections.bar.Menu;

public class Runner {
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.addDrink(new Drink("LaChouffe",3.50f, true));
        menu.addDrink(new Drink("Coca Cola",2.00f, false));
        menu.addDrink(new Drink("Spa Sparkling",2.00f, false));
        menu.addDrink(new Drink("Spa Still",2.00f, false));
        menu.addDrink(new Drink("Coca Cola Light",2.00f, false));
        menu.addDrink(new Drink("Coffee",2.50f, false));
        menu.addDrink(new Drink("Tea",2.50f, false));
        menu.addDrink(new Drink("Pils",2.00f, true));
        menu.addDrink(new Drink("Duvel",3.50f, true));
        menu.addDrink(new Drink("Orval",4.00f, true));

        System.out.printf("We have %d drinks on our menu, with a total cost of €%.2f\n\n", menu.getSize(), menu.getTotalPrice());
        System.out.println(menu);
        System.out.println("\nOur most expensive drink is: " + menu.getMostExpensiveDrink());
        System.out.println("\nOur alcoholic drinks are: " + menu.getAlcoholicDrinks());
        menu.removeMoreExpensiveThen();
        System.out.println("\nMenu without drinks above €3: " + menu);

        menu.addDrinks(new Drink[] { new Drink("Bushmills 10yr",7.00f, true), new Drink("SpringBank 5yr",5.00f, true)});

        System.out.println("\nExtended " + menu);

        menu.sort();

        System.out.println("\nSorted Menu: " + menu);

        menu.sortByName();

        System.out.println("\nSorted by name Menu: " + menu);

    }
}

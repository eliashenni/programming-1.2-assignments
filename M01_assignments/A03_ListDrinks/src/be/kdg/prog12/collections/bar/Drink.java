package be.kdg.prog12.collections.bar;

public class Drink implements Comparable <Drink> {
    private String name;
    private float price;
    private boolean isAlcoholic;

    public Drink(String name, float price, boolean isAlcoholic) {
        this.name = name;
        this.price = price;
        this.isAlcoholic = isAlcoholic;
    }


    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public boolean isAlcoholic() {
        return isAlcoholic;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setAlcoholic(boolean alcoholic) {
        isAlcoholic = alcoholic;
    }

    @Override
    public String toString() {
        return String.format("%s €%.2f", name, price);
    }

    @Override
    public int compareTo(Drink o) {
        int comparePrice = Double.compare(this.price, o.price);
        return comparePrice == 0 && isAlcoholic ? -1 : comparePrice;
    }
}

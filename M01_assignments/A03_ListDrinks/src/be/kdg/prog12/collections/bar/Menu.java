package be.kdg.prog12.collections.bar;

import java.util.*;

public class Menu {
    private List <Drink> drinks = new ArrayList<>();

    public void addDrink(Drink d) {
        drinks.add(d);
    }

    public int getSize() {
        return drinks.size();
    }

    public float getTotalPrice() {
        float total = 0.0f;
        for (Drink drink : drinks) {
            total += drink.getPrice();
        }
        return total;
    }

    public Drink getMostExpensiveDrink() {
        return Collections.max(drinks, Comparator.comparing(Drink::getPrice));
    }

    public List <Drink> getAlcoholicDrinks() {
        List <Drink> alcoholicDrinks= new ArrayList<>();
        for (Drink drink : drinks) {
            if (drink.isAlcoholic()) {
                alcoholicDrinks.add(drink);
            }
        }
        return alcoholicDrinks;
    }

    public void sort() {
        Collections.sort(drinks);
    }

    public void sortByName() {
        Collections.sort(drinks, new NameComparator());
    }

    public void removeMoreExpensiveThen() {
        drinks.removeIf(drink -> drink.getPrice() > 3.00f);
    }

    public void addDrinks(Drink[] d) {
        drinks.addAll(Arrays.asList(d));
    }

    @Override
    public String toString() {
        return "Menu: " + drinks;
    }


}

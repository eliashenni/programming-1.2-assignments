package listEvenNumbers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ListEvenNumbers {
    public static void main(String[] args) {
        List <Integer> list = new ArrayList<>();

        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            list.add(random.nextInt(50) + 1);
        }

        Collections.sort(list);
        System.out.println(list);

        Collections.reverse(list);
        System.out.println(list);

        Collections.shuffle(list);
        System.out.println(list);

        int frequency = Collections.frequency(list, 48);
        System.out.println("The number 48 appeared " + frequency + " times.");
    }
}

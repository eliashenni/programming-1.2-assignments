import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Brands {
    private ArrayList<String> brands = new ArrayList<> (List.of(Data.brands));

    public void alphabetic() {
        Collections.sort(brands);
    }

    public void alphabeticDescending() {
        Collections.sort(brands);
        Collections.reverse(brands);
    }

    @Override
    public String toString() {
        return brands.toString();
    }
}

package scores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Students {
    private List<Student> students;

    public Students(List<Student> students) {
        this.students = new ArrayList<>(students);
    }

    public void sortByScore(boolean sortByScore) {
        Collections.sort(students);
        if (!sortByScore) {
            Collections.reverse(students);
        }
    }

    public double getHighScore() {
        return Collections.max(students).getScore();
    }

    public double getLowScore() {
        return Collections.min(students).getScore();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Student s : students) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }
}

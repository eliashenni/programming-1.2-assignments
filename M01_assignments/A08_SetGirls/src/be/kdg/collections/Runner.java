package be.kdg.collections;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Runner {
    public static void main(String[] args) {
        List<Girl> girls = List.of(new Girl("An", 20), new Girl("Bea", 20),
                new Girl("Bea", 25), new Girl("Diana", 25),
                new Girl("Zoë", 18), new Girl("Ekaterina", 18),
                new Girl("Bea", 20));

        TreeSet<Girl> set = new TreeSet<>(girls);

        System.out.println("Set of " + set.size() + " girls: " + set);
        System.out.println("Last girl: " + set.last());
        System.out.println("Girl before Diana: " + set.lower(new Girl("Diana", 25)));
        System.out.println("Girl that would be after Dido (21): " + set.ceiling(new Girl("Dido", 21)));
    }
}

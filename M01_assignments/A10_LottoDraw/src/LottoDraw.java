import java.util.Random;
import java.util.TreeSet;

public class LottoDraw {
    public static void main(String[] args) {
        TreeSet<Integer> lotto = new TreeSet<>();

        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            lotto.add(random.nextInt(45) + 1);
        }

        System.out.print("The lotto numbers for today are: ");

        for (int i : lotto) {
            System.out.print(i + " ");
        }
    }
}

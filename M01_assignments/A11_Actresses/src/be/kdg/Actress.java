package be.kdg;

import java.util.Objects;

public class Actress implements Comparable<Actress>{
    private String name;
    private int birthYear;

    public Actress(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Actress)) return false;
        Actress actress = (Actress) o;
        return this.name.equals(actress.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Actress o) {
        int compareYear = this.birthYear - o.birthYear;
        return (compareYear != 0) ? compareYear : name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return String.format("%s (%d)", name, birthYear);
    }
}

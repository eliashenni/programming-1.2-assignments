package collections;

import java.util.*;

public class Runner {
    public static void main(String[] args) {
        List<Girl> girls = List.of(new Girl("An", 20), new Girl("Bea", 20),
                new Girl("Bea", 25), new Girl("Diana", 25),
                new Girl("Zoë", 18), new Girl("Ekaterina", 18),
                new Girl("Bea", 20));

        Map<String,Girl> map = new TreeMap<>();

        for (Girl g : girls) {
            map.put(g.getName(), g);
        }

        System.out.println("Map of " + map.size() + " girls: " + map.toString());
    }
}

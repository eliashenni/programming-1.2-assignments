package be.kdg.drivers;

import java.util.*;

public class Drivers {
    private Map<String, Driver> driverByName = new TreeMap<>();
    private Map<Integer, Driver> driverByYear = new TreeMap<>();

    public Drivers(String [][] champions, Integer [][] years) {
        for (int i = 0; i < champions.length; i++) {
            driverByName.put(champions[i][1], new Driver(champions[i][1], champions[i][0], new TreeSet<>(Set.of(years[i]))));
        }

        for (int i = 0; i < champions.length; i++) {
            for (int j = 0; j < years[i].length; j++) {
                driverByYear.put(years[i][j], new Driver(champions[i][1], champions[i][0], new TreeSet<>(Set.of(years[i]))));
            }
        }
    }
    public List<Driver> getDrivers() {
        return new ArrayList<>(driverByName.values());
    }

    public Driver getDriverByName(String name) {
        return driverByName.get(name);
    }

    public String getDriverByYear(int year) {
        return driverByYear.get(year) != null ? driverByYear.get(year).getName() : "Not Listed";
    }

    @Override
    public String toString() {
        return String.format("Drivers by name: \n%s\nDrivers by year: \n%s", driverByName, driverByYear);
    }
}

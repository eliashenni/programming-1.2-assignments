package inClass;

import java.util.Objects;

public class Month implements Comparable <Month> {
    private String name;
    private int number;
    private int days;

    public Month(String name, int number, int days) {
        this.name = name;
        this.number = number;
        this.days = days;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public int getDays() {
        return days;
    }

    @Override
    public String toString() {
        return name + " ";
    }

    @Override
    public int compareTo (Month o) {
        return number - o.number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Month month = (Month) o;

        if (number != month.number) return false;
        if (days != month.days) return false;
        return name != null ? name.equals(month.name) : month.name == null;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number, days);
    }
}

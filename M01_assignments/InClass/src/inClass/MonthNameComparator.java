package inClass;

import java.util.Comparator;

public class MonthNameComparator implements Comparator<Month>{

    @Override
    public int compare(Month o1, Month o2) {
        return o1.getName().compareTo(o2.getName());
    }
}

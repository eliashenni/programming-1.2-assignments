package inClass;

import java.util.*;

public class Sort {
    public static void main(String[] args) {
        List <Month> months = new ArrayList<>(
                List.of(new Month("february", 2, 28),
                        new Month("september", 9, 30),
                        new Month("january", 1, 31),
                        new Month("june", 6, 30),
                        new Month("july", 7, 31),
                        new Month("august", 8, 31),
                        new Month("december", 12, 31)));

//        Collections.sort(months);
//        System.out.println("Sorted order: " + months);
//        Collections.sort(months, new MonthNameComparator());
//        System.out.println("Sorted alpha: " + months);
//        Collections.reverse(months);
//        System.out.println("Reversed: " + months);

        // Sets
        Set <Month> monthSet = new HashSet<>(months);
        System.out.printf("%d months: %s\n", monthSet.size(), monthSet);
        monthSet.add(new Month("february", 2, 29));

        System.out.printf("more months: %d months: %s\n", monthSet.size(), monthSet);

    }
}

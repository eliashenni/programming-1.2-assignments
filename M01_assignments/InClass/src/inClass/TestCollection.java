package inClass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestCollection {
    public static void main(String[] args) {
        List <Integer> numbers = new ArrayList<>(); // A list is a Collection
        numbers.add(4);
        numbers.add(8);
        numbers.add(1);
        numbers.add(9);
        numbers.add(2);
        numbers.add(7);

        Collections.sort(numbers);
        for (Integer number : numbers) {
            System.out.println(number);
        }

        System.out.println();

        List<String> test = new ArrayList<>();
        test.add("9");
        test.add("10");
        test.add("1");
        Collections.sort(test);
        for (String s : test) {
            System.out.println(s);
        }

    }
}

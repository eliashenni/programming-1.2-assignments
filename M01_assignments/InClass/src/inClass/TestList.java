package inClass;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestList {
    public static void main(String[] args) {
        //int[] numbers = new int[20];
        List<String> names = new ArrayList <>(/*20*/); //default size is ten
        names.add("Lars"); // 0
        names.add("Jan"); // 1
        names.add("Jan"); // 1
        names.add("Levente"); // 2
        names.add(3, "Elias"); // 3

        for (int i = 0; i < names.size(); i++) {
            String name = names.get(i);
            System.out.println(name);
        }

        System.out.println();

        for (String n : names) {
            System.out.println(n);
        }

        int index = names.indexOf("Levente");
        System.out.println("Index of Levente: " + index);

        names.remove("Jan");

        System.out.println();
        for (String n : names) {
            System.out.println(n);
        }


        List<Object> names3 = new ArrayList<>();
        names3.add(3); //primitive int >> Integer
        names3.add(3.5); //primitive double >> Double

        // wrapper classes that are associated
        // boxing
        Integer number1 = new Integer(1);
        Float float1 = new Float(1.2f);
        Boolean b = new Boolean(true);

        // creates an instance of the 'Integer' class
        Integer number2 = 2;
        Float float2 = 2.5f;
        Boolean b2 = true;

        // unboxing
        int number3 = number2;


        List <String> name2 = new LinkedList<>();

    }
}
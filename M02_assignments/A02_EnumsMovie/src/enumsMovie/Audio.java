package enumsMovie;

public enum Audio {
    PCM("PCM"), DOLBY("Dolby"), DOLBY_HD("Dolby HD")
    , VHS("VHS"), DTS_HD("DTS HD");

    private final String humanReadable;

    Audio(String humanReadable) {
        this.humanReadable = humanReadable;
    }

    @Override
    public String toString() {
        return humanReadable;
    }
}

package enumsMovie;

public enum Format {
    VHS("VHS"), DVD("DVD"), BLU_RAY("Blu-ray");

    private final String humanReadable;

    Format(String humanReadable) {
        this.humanReadable = humanReadable;
    }

    @Override
    public String toString() {
        return humanReadable;
    }
}

package enumsMovie;

public class Movie {
    private final String title;
    private final int year;
    private final Format format;
    private final Audio audio;

    public Movie(String title, int year, Format format, Audio audio) {
        this.title = title;
        this.year = year;
        this.format = format;
        this.audio = audio;
    }

    @Override
    public String toString() {
        return String.format("%-20s %d %-10s %s", title, year, format, audio);
    }
}

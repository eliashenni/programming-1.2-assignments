package enumsMovie;

import java.util.LinkedList;
import java.util.List;

public class TestMovies {
    public static void main(String[] args) {
        List <Movie> movieList = new LinkedList<>();
        movieList.add(new Movie("Ronin", 1998, Format.DVD, Audio.DOLBY));
        movieList.add(new Movie("Lakeview Terrace", 2008, Format.BLU_RAY, Audio.DOLBY_HD));
        movieList.add(new Movie("Ghost Town", 2008, Format.DVD, Audio.DOLBY));
        movieList.add(new Movie("Stealth", 2005, Format.VHS, Audio.VHS));
        movieList.add(new Movie("Fast & Furious 6", 2013, Format.BLU_RAY, Audio.DTS_HD));
        movieList.add(new Movie("Twilight", 2008, Format.DVD, Audio.DOLBY));
        movieList.add(new Movie("The Brave One", 2007, Format.VHS, Audio.VHS));

        for (Movie e : movieList) {
            System.out.println(e.toString());
        }
    }
}

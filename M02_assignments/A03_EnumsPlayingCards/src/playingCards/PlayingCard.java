package playingCards;

import java.util.Random;

public class PlayingCard {
    private static Random random = new Random();
    private Rank rank;
    private Suit suit;

    public PlayingCard(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public static PlayingCard generateRandomPlayingCard() {
        Suit[] suit = Suit.values();
        Rank[] rank = Rank.values();
        return new PlayingCard(rank[random.nextInt(rank.length)], suit[random.nextInt(suit.length)]);
    }

    @Override
    public String toString() {
        return String.format("%s of %s", rank, suit);
    }

    public int getValue() {
        return rank.getValue();
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public enum Rank {
        TWO(2, "two"), THREE(3, "three"), FOUR(4, "four"), FIVE(5, "five"),
        SIX(6, "six"), SEVEN(7, "seven"), EIGHT(8, "eight"), NINE(9, "nine"),
        TEN(10, "ten"), JACK(10, "jack"), QUEEN(10, "queen"), KING(10, "king"), ACE(1, "ace");

        private final int value;
        private final String name;

        Rank(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int getValue() {
            return value;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum Suit {
        HEARTS("hearts"), SPADES("spades"), CLUBS("clubs"), DIAMONDS("diamonds");

        private final String name;

        Suit(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}

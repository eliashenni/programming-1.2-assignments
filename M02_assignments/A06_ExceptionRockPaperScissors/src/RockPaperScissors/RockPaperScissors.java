package RockPaperScissors;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean playGame = true;

        do {
            System.out.print("Please enter 'rock', 'paper', or 'scissors' (or 'stop'): ");
            String input = scanner.next();

            switch (input) {
                case "rock":
                case "scissors":
                case "paper":
                    break;
                case "stop":
                    playGame = false;
                    break;
                default:
                    throw new InputMismatchException("Input is invalid.");
            }

            if (playGame) {
                Random random = new Random();
                int computer = random.nextInt(3);
                String computerS = "";
                switch (computer) {
                    case 0:
                        computerS = "rock";
                        break;
                    case 1:
                        computerS = "paper";
                        break;
                    case 2:
                        computerS = "scissors";
                        break;
                }
                System.out.println("Computer chose " + computerS);

                if (input.equals(computerS)) {
                    System.out.println("It's a tie!");
                } else if ((input.equals("rock") && computerS.equals("paper")) ||
                        (input.equals("paper") && computerS.equals("scissors")) ||
                        (input.equals("scissors") && computerS.equals("rock"))) {
                    System.out.println("\tComputer won");
                } else {
                    System.out.println("You won, congratulations!");
                }
            }
        } while (playGame);
    }
}

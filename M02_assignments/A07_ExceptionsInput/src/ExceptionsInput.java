import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionsInput {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number 1 - 10: ");
        int number = 0;
        boolean validNumber = false;

        while (!validNumber) {
            try {
                number = scanner.nextInt();
                if (number < 1 || number > 10) {
                    throw new InputMismatchException(number + " is not a number from 1 - 10");
                }
                validNumber = true;
            } catch (InputMismatchException e) {
                scanner.nextLine();
                System.out.println(e.getMessage());
                System.out.print("Enter a number 1 - 10: ");
            }
        }
        System.out.println(number + " is a valid number!");
    }
}

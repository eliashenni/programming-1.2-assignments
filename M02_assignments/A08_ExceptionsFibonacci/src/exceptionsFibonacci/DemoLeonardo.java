package exceptionsFibonacci;

public class DemoLeonardo {
    public static void main(String[] args) {
        try {
            Fibonacci.fibonacciNumber(-1);
        } catch (FibonacciException e) {
            System.out.println(e.getMessage());
        }
        try {
            for (int i = 0; i < 100; i++) {
                double dividend = Fibonacci.fibonacciNumber(i + 1);
                long divisor = Fibonacci.fibonacciNumber(i);
                System.out.printf("f(%d) / f(%d) = %.15f%n", i + 1,
                        i, dividend / divisor);
            }
        } catch (FibonacciException e) {
            System.out.println(e.getMessage());
        }

    }
}

package exceptionsFibonacci;

public class Fibonacci {
    private static final long MAX = 91;

    public static long fibonacciNumber(int n) {
        if (n < 0) {
            throw new FibonacciException("Can't pass a negative number!");
        } else if (n > MAX) {
            throw new FibonacciException("Can't pass a number greater then " + MAX);
        }
        long first = 0;
        long second = 1;
        long number = 0;

        for (int i = 0; i < n; i++) {
            number = first + second;
            first = second;
            second = number;
        }

        return number;
    }
}

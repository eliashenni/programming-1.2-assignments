package exceptionsFibonacci;

public class FibonacciException extends ArithmeticException {
    public FibonacciException(String s) {
        super(s);
    }
}

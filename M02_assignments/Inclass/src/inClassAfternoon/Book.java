package inClassAfternoon;

public class Book extends Product {
    private final String author;
    private final String title;

    public Book(String code, String description, double price, String author, String title) {
        super(code, description, price);
        this.author = author;
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                "} " + super.toString();
    }
}

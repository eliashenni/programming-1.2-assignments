package inClassAfternoon;

public class DemoCallStack {
    public static void main(String[] args) {
        try {
            Product lotr = new Book("TT", "A 20th century classic",
                    19.90, "J.R.R. Tolkien",
                    "The Two Towers");
            System.out.println("Book: " + lotr);
        } catch (ECommerceException  e ) {
            System.err.println("An error occurred");
            e.printStackTrace();
        }
        System.out.println("Ending the program");
    }
}

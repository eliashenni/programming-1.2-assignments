package inClassAfternoon;

public class ECommerceException extends RuntimeException {
    public ECommerceException (String s) {
        super(s);
    }
    public ECommerceException() {
        super();
    }
}

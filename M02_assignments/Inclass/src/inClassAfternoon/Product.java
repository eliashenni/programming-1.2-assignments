package inClassAfternoon;

public class Product {
    protected final String code;
    protected final String description;
    protected final double price;

    public Product(String code, String description, double price) {
        if (code.length() < 3) {
            throw new IllegalArgumentException("Thou will not pass!");
        }
        this.code = code;
        this.description = description;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}

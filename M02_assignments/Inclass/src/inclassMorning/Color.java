package inclassMorning;

public enum Color {
    WHITE("FFFFFF"), BLACK("000000"), RED("FF0000"), GREEN("00FF00")
    , BLUE ("0000FFF");

    private final String hexCode;

    Color(String hexCode) {
        this.hexCode = hexCode;

        // TOTALLY not possible
        //Color color = new Color("#FF0000")
    }

    public String getHexCode() {
        return hexCode;
    }

    public static Color of(String hexCode) {
        for (Color color : values()) {
            if (color.getHexCode().equals(hexCode)) {
                return color;
            }
        }
        return null;
    }
}

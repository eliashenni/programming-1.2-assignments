package inclassMorning;

public class inclass {
    public static final int BLACK = 1;
    public static final int WHITE = 2;
    public static final int RED = 3;
    public static final int GREEN = 4;
    public static final int BLUE = 5;
    public static final int ORANGE = 6;

    public static final String DEFAULT_FILE_PATH = "C:\\Windows\\";

    public static void main(String[] args) {
        Color color;

        //
        System.out.println("Please choose a color: ");

        color = Color.BLUE;
        color = Color.RED;

        System.out.println("BLUE: " + Color.BLUE.toString());

        if (color instanceof Color) {
            System.out.println("Yes, it's an instance of a color");
        }

        System.out.println("BLUE name" + Color.BLUE.name());
        System.out.println("GREEN ordinal" + Color.GREEN.ordinal());

    }
}

package outerClass;

public class OuterClass {
    private int value;
    private String stringValue;

    void takeSomeOtherAction() {
        InnerClass ic = this.new InnerClass();
        ic.innerValue++;
    }

    // Regular: so it's tied to an INSTANCE of the outer class
    public class InnerClass {
        private int innerValue;

        void takeAction() {
            innerValue++;
            value++;
        }
    }
}

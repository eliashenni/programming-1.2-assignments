package onStage;

import javafx.application.Application;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class OnStage extends Application {
    @Override
    public void start(Stage stage) {
        Scene scene = new Scene(new Group());

        scene.setCursor(Cursor.CROSSHAIR);
        scene.setFill(Color.YELLOW);
        stage.setScene(scene);

        stage.setMinWidth(500.0);
        stage.setMinHeight(600.0);
        stage.setResizable(false);
        stage.setTitle("On Stage");
        stage.setResizable(true);
        stage.setMaxWidth(700.0);
        stage.show();

        Stage stage1 = new Stage();
        stage1.setTitle("New Stage");
        stage1.show();
        Scene groupScene = new Scene(new Group());



    }
}

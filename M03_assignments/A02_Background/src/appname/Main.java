package appname;

import appname.model.Background;
import appname.view.BackgroundPresenter;
import appname.view.BackgroundView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        Background model = new Background();
        BackgroundView view = new BackgroundView();
        new BackgroundPresenter(model, view);
        primaryStage.setScene(new Scene(view));
        primaryStage.setTitle("Background");
        primaryStage.setMinWidth(400);
        primaryStage.setMinHeight(250);
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}


package appname.model;

import java.util.Random;

public class Background {

    // private attributes
    private final int MAX_COLOR = 256;
    private String backgroundColor = "cornsilk";

    public Background() {
        // Constructor
    }

    // methods with business logic

    public void setRandomColor() {
        Random random = new Random();
        backgroundColor = String.format("rgb(%d,%d,%d)", random.nextInt(MAX_COLOR), random.nextInt(MAX_COLOR), random.nextInt(MAX_COLOR));
    }

    // needed getters and setters

    public String getBackgroundColor() {
        return backgroundColor;
    }
}


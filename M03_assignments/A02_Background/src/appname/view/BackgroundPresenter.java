package appname.view;

import appname.model.Background;

public class BackgroundPresenter {
    private Background model;
    private BackgroundView view;

    public BackgroundPresenter(
            Background model, BackgroundView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        // Adds event handlers (inner classes or lambdas)
        // to view controls
        // Event handlers: call model methods and
        // update the view.
        view.getButton().setOnAction(event -> {
            model.setRandomColor();
            updateView();
        });
    }

    private void updateView() {
        // fills the view with model data
        view.setStyle(String.format("-fx-background-color:%s;", model.getBackgroundColor()));
    }
}
package appname.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public class BackgroundView extends BorderPane
        /* layout type */ {
    // private Node attributes (controls)
    private Button button;

    public BackgroundView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        // button = new Button("...")
        // label = new Label("...")
        button = new Button("Repaint");
    }

    private void layoutNodes() {
        // add/set … methodes
        // Insets, padding, alignment, …
        setBottom(button);
        setAlignment(button, Pos.BOTTOM_RIGHT);
        setPadding(new Insets(10, 10, 10, 10));

    }

    // package-private Getters
    // for controls used by Presenter

    Button getButton() {
        return button;
    }
}


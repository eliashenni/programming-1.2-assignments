package appname;

import appname.model.Reverse;
import appname.view.ReversePresenter;
import appname.view.ReverseView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        Reverse model = new Reverse("Enter a text here");
        ReverseView view = new ReverseView();
        new ReversePresenter(model, view);
        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}


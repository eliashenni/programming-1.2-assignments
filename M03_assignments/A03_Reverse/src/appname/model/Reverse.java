package appname.model;

public class Reverse {
    // private attributes
    private String text;

    public Reverse(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    // methods with business logic

    public void reverse() {
        StringBuilder sb = new StringBuilder(text);
        sb.reverse();
        text = sb.toString();
    }
    // needed getters and setters
}


package appname.view;

import appname.model.Reverse;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ReversePresenter {
    private Reverse model;
    private ReverseView view;

    public ReversePresenter(
            Reverse model, ReverseView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        // Adds event handlers (inner classes or lambdas)
        // to view controls
        // Event handlers: call model methods and
        // update the view.
        view.getEnterButton().setOnAction(new ReverseHandler());
    }

    public class ReverseHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            model.setText(view.getTextField().getText());
            model.reverse();
            updateView();
        }
    }

    private void updateView() {
        // fills the view with model data
        view.getTextField().setText(model.getText());
    }
}
package appname.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class ReverseView extends GridPane
        /* layout type */ {
    // private Node attributes (controls)
    private Button enterButton;
    private TextField textField;

    public ReverseView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        // button = new Button("...")
        // label = new Label("...")
        enterButton = new Button("reverse");
        textField = new TextField();
    }

    private void layoutNodes() {
        // add/set … methodes
        // Insets, padding, alignment, …
        add(textField, 0, 0);
        add(enterButton,0,1);
        setHalignment(enterButton, HPos.RIGHT);
        setVgap(10);
        setHgap(10);
        setPadding(new Insets(10,10,10,10));
    }

    // package-private Getters
    // for controls used by Presenter

    Button getEnterButton() {
        return enterButton;
    }

    TextField getTextField() {
        return textField;
    }
}


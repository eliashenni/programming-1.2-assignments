package be.kdg.dice.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class DiceView extends GridPane {
	private ImageView dice1;
	private ImageView dice2;
	private Button button;

	ImageView getDice1() {
		return dice1;
	}

	ImageView getDice2() {
		return dice2;
	}

	Button getButton() {
		return button;
	}

	public DiceView() {
		initialiseNodes();
		layoutNodes();
	}

	private void layoutNodes() {
		setVgap(10);
		setHgap(10);
		setPadding(new Insets(10));

		add(dice1,0,0);
		add(dice2,1,0);
		add(button, 0, 1, 2, 1);
		setHalignment(button, HPos.CENTER);
	}

	private void initialiseNodes() {
		dice1 = new ImageView();
		dice2 = new ImageView();
		button = new Button("Roll");
		button.setPrefWidth(80);
	}
}

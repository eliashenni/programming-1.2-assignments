package be.kdg.dice.view;

import be.kdg.dice.model.Dice;
import javafx.scene.image.Image;

public class Presenter {
    private Dice model;
    private DiceView view;

    public Presenter(Dice model, DiceView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getButton().setOnAction(event -> {
            model.werp();
            updateView();
        });
    }

    private void updateView() {
        view.getDice1().setImage(new Image("die" + model.getNumberOfPips1() + ".png"));
        view.getDice2().setImage(new Image("die" + model.getNumberOfPips2() + ".png"));
    }
}

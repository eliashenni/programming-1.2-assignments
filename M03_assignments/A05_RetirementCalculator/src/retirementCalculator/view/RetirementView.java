package retirementCalculator.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;

public class RetirementView extends GridPane {
    private Label enterYear;
    private Label retirementYear;
    private TextField input;
    private Button birthYearButton;
    private Label showRetirementYear;

    public RetirementView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        enterYear = new Label("Birth Year");
        retirementYear = new Label("Retirement Year");
        input = new TextField();
        input.setPrefColumnCount(4);
        birthYearButton = new Button("BirthYear > RetirementYear");
        showRetirementYear = new Label();
    }

    private void layoutNodes() {
        this.add(enterYear, 0,0);
        this.add(input, 0,1);
        this.add(birthYearButton, 1,0, 1, 2);
        this.add(retirementYear, 2,0 );
        this.add(showRetirementYear, 2 ,1);

        setMargin(input, new Insets(10));
        setMargin(birthYearButton, new Insets(10));
        setMargin(showRetirementYear, new Insets(10));

        this.setPadding(new Insets(10));
    }

    TextField getInput() {
        return input;
    }

    Button getBirthYearButton() {
        return birthYearButton;
    }

    Label getShowRetirementYear() {
        return showRetirementYear;
    }
}


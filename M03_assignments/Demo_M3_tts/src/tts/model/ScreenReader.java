package tts.model;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

public class ScreenReader {
    private String text;
    private String freeTTSVoice;

    public ScreenReader() {
        initialiseFreeTTS();
    }

    private void initialiseFreeTTS() {
        System.setProperty("freetts.voices",
                "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
        freeTTSVoice = "kevin16";
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void readAloud() {
        // System.out.println("TEXT: " + text);

        Voice voice = VoiceManager.getInstance().getVoice(freeTTSVoice);
        voice.allocate();
        voice.speak(text);
        voice.deallocate();
    }
}

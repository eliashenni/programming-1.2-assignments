package tts.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class TTSEventHandler implements EventHandler<ActionEvent> {
    @Override
    public void handle(ActionEvent actionEvent) {
        System.out.println("Button has been pressed");
    }
}

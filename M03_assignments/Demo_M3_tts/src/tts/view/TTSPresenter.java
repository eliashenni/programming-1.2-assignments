package tts.view;

import tts.model.ScreenReader;

public class TTSPresenter {
    private final ScreenReader model;
    private final TTSView view;

    public TTSPresenter(ScreenReader model, TTSView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        this.view.getReadButton().setOnAction((/*ActionEvent*/ event) -> {
                    this.model.setText(this.view.getUserText().getText());
                    this.model.readAloud();
                }
        );
    }

    private void updateView() {
        // Nothing to do yet
    }
}

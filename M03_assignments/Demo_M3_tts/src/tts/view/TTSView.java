package tts.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

public class TTSView extends BorderPane {
    // Each node (UI widget) is added as attribute to the view
    private Button readButton;
    private TextArea userText;

    public TTSView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        readButton = new Button("Read aloud");
        userText = new TextArea();
    }

    private void layoutNodes() {
        readButton.setPrefWidth(120.0);
        readButton.setPrefHeight(20.0);

        this.setCenter(userText);
        this.setBottom(readButton);

        BorderPane.setAlignment(readButton, Pos.CENTER);
        BorderPane.setMargin(readButton, new Insets(10.0));
        BorderPane.setMargin(userText, new Insets(10.0));
    }

    // package private
    Button getReadButton() {
        return readButton;
    }

    TextArea getUserText() {
        return userText;
    }
}

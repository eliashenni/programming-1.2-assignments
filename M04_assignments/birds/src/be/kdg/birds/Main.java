package be.kdg.birds;

import be.kdg.birds.view.BirdsView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage stage){
        BirdsView birdsView = new BirdsView();
        stage.setTitle("Birds");
        stage.setScene(new Scene(birdsView));
        stage.setMinWidth(400);
        stage.setMinHeight(300);
        stage.getIcons().add(new Image("/angrybird.png"));
        stage.show();

    }
}

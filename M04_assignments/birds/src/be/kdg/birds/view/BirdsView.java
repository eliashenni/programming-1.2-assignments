package be.kdg.birds.view;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class BirdsView extends BorderPane {
    private MenuItem menuItem;
    private CheckBox checkBox;
    private Image birdImage;

    public BirdsView() {
        initializeNodes();
        layoutNodes();
    }

    public void initializeNodes() {
        birdImage = new Image("/angrybird.png");

        menuItem = new MenuItem("Menu Item", new ImageView(birdImage));
        checkBox = new CheckBox();
    }

    public void layoutNodes() {
        Menu menu = new Menu("Menu");
        menu.getItems().add(menuItem);
        menu.setGraphic(new ImageView(birdImage));

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().add(menu);

        this.setTop(menuBar);

        checkBox.setGraphic(new ImageView(birdImage));
        this.setCenter(checkBox);

    }
}

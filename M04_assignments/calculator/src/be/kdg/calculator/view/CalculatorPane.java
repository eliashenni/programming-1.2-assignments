package be.kdg.calculator.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class CalculatorPane extends GridPane {
    private static final double BUTTON_SIZE = 35.0;
    private static final double GAP = 5.0;
    private TextField textField;
    private Button zero;
    private Button one;
    private Button two;
    private Button three;
    private Button four;
    private Button five;
    private Button six;
    private Button seven;
    private Button eight;
    private Button nine;
    private Button point;
    private Button plus;
    private Button minus;
    private Button multiplication;
    private Button divide;
    private Button clear;
    private Button equals;

    public CalculatorPane() {
        initialiseNodes();
        layoutNodes();
    }

    public void initialiseNodes() {
        zero = new Button("0");
        one = new Button("1");
        two = new Button("2");
        three = new Button("3");
        four = new Button("4");
        five = new Button("5");
        six = new Button("6");
        seven = new Button("7");
        eight = new Button("8");
        nine = new Button("9");
        point = new Button(".");

        plus = new Button("+");
        minus = new Button("-");
        multiplication = new Button("X");
        divide = new Button("÷");

        clear = new Button("C");
        equals = new Button("=");

        textField = new TextField();
        textField.setAlignment(Pos.CENTER_RIGHT);
        textField.setEditable(false);
    }

    public void layoutNodes() {
        this.setPadding(new Insets(GAP, GAP, GAP, GAP));
        this.setHgap(GAP);
        this.setVgap(GAP);

        zero.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        one.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        two.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        three.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        four.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        five.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        six.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        seven.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        eight.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        nine.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        point.setMinSize(BUTTON_SIZE, BUTTON_SIZE);

        plus.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        minus.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        multiplication.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        divide.setMinSize(BUTTON_SIZE, BUTTON_SIZE);

        clear.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        equals.setMinSize(BUTTON_SIZE, BUTTON_SIZE);
        textField.setMinSize(BUTTON_SIZE, BUTTON_SIZE);

        zero.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        one.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        two.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        three.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        four.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        five.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        six.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        seven.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        eight.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        nine.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        point.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        plus.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        minus.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        multiplication.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        divide.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        clear.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        equals.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        textField.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);


        GridPane.setConstraints(textField, 0,0, 5, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(textField,0,0);
        GridPane.setConstraints(seven, 0,1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(seven,0,1);
        GridPane.setConstraints(eight, 1,1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(eight,1,1);
        GridPane.setConstraints(nine, 2,1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(nine,2,1);
        GridPane.setConstraints(divide, 3,1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(divide,3,1);
        GridPane.setConstraints(clear, 4,1, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(clear,4,1);
        GridPane.setConstraints(four, 0,2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(four,0,2);
        GridPane.setConstraints(five, 1,2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(five,1,2);
        GridPane.setConstraints(six, 2,2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(six,2,2);
        GridPane.setConstraints(multiplication, 3,2, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(multiplication,3,2);
        GridPane.setConstraints(equals, 4,2, 1, 3, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(equals,4,2);
        GridPane.setConstraints(one, 0,3, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(one,0,3);
        GridPane.setConstraints(two, 1,3, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(two,1,3);
        GridPane.setConstraints(three, 2,3, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(three,2,3);
        GridPane.setConstraints(minus, 3,3, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(minus,3,3);
        GridPane.setConstraints(zero, 0,4, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(zero,0,4);
        GridPane.setConstraints(point, 1,4, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(point,1,4);
        GridPane.setConstraints(plus, 3,4, 1, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.ALWAYS);
        this.add(plus,3,4);
    }

    TextField getTextField() {
        return textField;
    }

    Button getZero() {
        return zero;
    }

    Button getOne() {
        return one;
    }

    Button getTwo() {
        return two;
    }

    Button getThree() {
        return three;
    }

    Button getFour() {
        return four;
    }

    Button getFive() {
        return five;
    }

    Button getSix() {
        return six;
    }

    Button getSeven() {
        return seven;
    }

    Button getEight() {
        return eight;
    }

    Button getNine() {
        return nine;
    }

    Button getPoint() {
        return point;
    }

    Button getPlus() {
        return plus;
    }

    Button getMinus() {
        return minus;
    }

    Button getMultiplication() {
        return multiplication;
    }

    Button getDivide() {
        return divide;
    }

    Button getClear() {
        return clear;
    }

    Button getEquals() {
        return equals;
    }
}

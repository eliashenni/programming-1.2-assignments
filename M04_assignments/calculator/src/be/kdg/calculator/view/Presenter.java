package be.kdg.calculator.view;

import be.kdg.calculator.model.Calculator;
import be.kdg.calculator.model.OperandCharacter;
import be.kdg.calculator.model.Operator;

public class Presenter {
    private Calculator model;
    private CalculatorPane view;

    public Presenter(Calculator model, CalculatorPane view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    public void addEventHandlers() {
        this.view.getZero().setOnAction(event -> {this.model.handleInput(OperandCharacter.ZERO); updateView();});
        this.view.getOne().setOnAction(event -> {this.model.handleInput(OperandCharacter.ONE); updateView();});
        this.view.getTwo().setOnAction(event -> {this.model.handleInput(OperandCharacter.TWO); updateView();});
        this.view.getThree().setOnAction(event -> {this.model.handleInput(OperandCharacter.THREE); updateView();});
        this.view.getFour().setOnAction(event -> {this.model.handleInput(OperandCharacter.FOUR); updateView();});
        this.view.getFive().setOnAction(event -> {this.model.handleInput(OperandCharacter.FIVE); updateView();});
        this.view.getSix().setOnAction(event -> {this.model.handleInput(OperandCharacter.SIX); updateView();});
        this.view.getSeven().setOnAction(event -> {this.model.handleInput(OperandCharacter.SEVEN); updateView();});
        this.view.getEight().setOnAction(event -> {this.model.handleInput(OperandCharacter.EIGHT); updateView();});
        this.view.getNine().setOnAction(event -> {this.model.handleInput(OperandCharacter.NINE); updateView();});
        this.view.getPoint().setOnAction(event -> {this.model.handleInput(OperandCharacter.DECIMAL_SEPARATOR); updateView();});

        this.view.getPlus().setOnAction(event -> {this.model.handleInput(Operator.PLUS); updateView();});
        this.view.getMinus().setOnAction(event -> {this.model.handleInput(Operator.MINUS); updateView();});
        this.view.getDivide().setOnAction(event -> {this.model.handleInput(Operator.DIVIDE); updateView();});
        this.view.getMultiplication().setOnAction(event -> {this.model.handleInput(Operator.MULTIPLY); updateView();});

        this.view.getEquals().setOnAction(event -> {this.model.calculate(); updateView();});
        this.view.getClear().setOnAction(event -> {this.model.clear(); updateView();});

    }



    public void updateView() {
        this.view.getTextField().setText(this.model.getDisplay());
    }

}

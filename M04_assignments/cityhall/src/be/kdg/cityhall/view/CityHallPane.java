package be.kdg.cityhall.view;

import javafx.geometry.Insets;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class CityHallPane extends VBox {
    private static final Image IMAGE = new Image("/cityhall.jpg");

    private ImageView imageView;
    private RadioButton normal;
    private RadioButton blackAndWhite;
    private RadioButton sepia;

    public CityHallPane() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    public void initialiseNodes() {
        ToggleGroup colors = new ToggleGroup();
        this.imageView = new ImageView(IMAGE);

        this.normal = new RadioButton("Normal");
        this.normal.setToggleGroup(colors);
        this.normal.setSelected(true);

        this.blackAndWhite = new RadioButton("Black and White");
        this.blackAndWhite.setToggleGroup(colors);

        this.sepia = new RadioButton("Sepia");
        this.sepia.setToggleGroup(colors);

        this.setSpacing(15.0);
        this.setPadding(new Insets(10));
    }

    public void layoutNodes() {
        this.getChildren().add(0, imageView);
        this.getChildren().add(1, normal);
        this.getChildren().add(2, blackAndWhite);
        this.getChildren().add(3, sepia);
    }

    void resetEffect() {
        this.imageView.setEffect(null);
    }

    void applyBlackAndWhiteEffect() {
        ColorAdjust blackAndWhite = new ColorAdjust();
        blackAndWhite.setSaturation(-1.0);
        this.imageView.setEffect(blackAndWhite);
    }

    void applySepiaEffect() {
        SepiaTone sepiaTone = new SepiaTone();
        sepiaTone.setLevel(0.8);
        this.imageView.setEffect(sepiaTone);
    }

    RadioButton getNormal() {
        return normal;
    }

    RadioButton getBlackAndWhite() {
        return blackAndWhite;
    }

    RadioButton getSepia() {
        return sepia;
    }
}

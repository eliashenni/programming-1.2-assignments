package be.kdg.cityhall.view;

public class Presenter {
    private final CityHallPane view;

    public Presenter(CityHallPane view) {
        this.view = view;

        this.addEventHandlers();
    }

    private void addEventHandlers() {
        this.view.getNormal().setOnAction(event -> {
            this.view.resetEffect();
        });

        this.view.getBlackAndWhite().setOnAction(event -> {
            this.view.applyBlackAndWhiteEffect();
        });

        this.view.getSepia().setOnAction(event -> {
            this.view.applySepiaEffect();
        });
    }
}

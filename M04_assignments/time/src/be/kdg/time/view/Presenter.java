package be.kdg.time.view;

import be.kdg.time.model.TimeModel;

import java.time.LocalTime;

public class Presenter {
    private TimeModel model;
    private TimeView view;

    public Presenter(TimeModel model, TimeView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    public void addEventHandlers() {
        view.getSlider().setOnMouseDragged(mouseEvent -> {
            double total = view.getSlider().getValue();
            double minutes = total % 1;
            double hours = total - minutes;
            minutes = minutes * 60;

            this.model.setCurrentTime(LocalTime.of((int)hours, (int)minutes));

            updateView();
        });
    }

    public void updateView() {
        this.view.applyDaylightSun(model.getDaylightPercentage(), model.getSunHeight(), model.getSunPositionX());

        LocalTime time = this.model.getCurrentTime();
        this.view.getSlider().setValue(time.getHour() + (time.getMinute() / 60.0));
    }


}

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Grid;
import view.GridPresenter;
import view.GridView;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage){
        Grid model = new Grid(10,10);
        GridView view = new GridView();
        GridPresenter presenter = new GridPresenter(model, view);

        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }
}

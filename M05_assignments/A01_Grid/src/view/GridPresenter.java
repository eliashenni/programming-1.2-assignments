package view;

import model.Grid;

public class GridPresenter {
    Grid model;
    GridView view;

    public GridPresenter(Grid model, GridView view) {
        this.model = model;
        this.view = view;

        addEventHandler();
        updateView();
    }

    public void addEventHandler() {
        this.view.getRowsSlider().setOnMouseDragged(mouseEvent -> {
            this.model.setRows((int)this.view.getRowsSlider().getValue());
            updateView();
        });

        this.view.getColumnsSlider().setOnMouseDragged(mouseEvent -> {
            this.model.setColumns((int)this.view.getColumnsSlider().getValue());
            updateView();
        });
    }

    public void updateView() {
        this.view.getRowsSlider().setValue(this.model.getRows());
        this.view.getColumnsSlider().setValue(this.model.getColumns());
        
        this.view.drawGrid(this.model.getColumns(), this.model.getRows());
    }
}

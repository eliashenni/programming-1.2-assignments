package view;

import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class GridView extends BorderPane {
    private Canvas canvas;
    private Label rows;
    private Label columns;
    private Slider rowsSlider;
    private Slider columnsSlider;

    public GridView() {
        initialiseNodes();
        layourNodes();
    }

    public void initialiseNodes() {
        canvas = new Canvas(640,400);
        rows = new Label("Rows");
        columns = new Label("Columns");
        rowsSlider = new Slider(2.0, 20.0, 10.0);
        rowsSlider.setShowTickLabels(true);
        rowsSlider.setShowTickMarks(true);
        columnsSlider = new Slider(2.0, 20.0, 10.0);
        columnsSlider.setShowTickMarks(true);
        columnsSlider.setShowTickLabels(true);
    }

    public void layourNodes() {
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER_LEFT);
        vBox.getChildren().add(0,rows);
        vBox.getChildren().add(1,rowsSlider);
        vBox.getChildren().add(2,columns);
        vBox.getChildren().add(3,columnsSlider);

        this.setCenter(canvas);
        this.setBottom(vBox);
    }

    Slider getRowsSlider() {
        return rowsSlider;
    }

    Slider getColumnsSlider() {
        return columnsSlider;
    }

    public void drawGrid(int columns, int rows) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        double width = canvas.getWidth();
        double height = canvas.getHeight();
        double rowHeight = height / rows;
        double colWidth = width / columns;

        gc.setFill(Color.WHITE);
        gc.fillRect(0.0,0.0, width, height);

        for (int i = 1; i < columns; i++) {
            gc.strokeLine(i * colWidth, 0, i * colWidth, height);
        }
        for (int i = 1; i < rows; i++) {
            gc.strokeLine(0 , i * rowHeight, width, i * rowHeight);
        }
    }
}

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Survey;
import view.PiePresenter;
import view.PieView;

public class PieMain extends Application {
    @Override
    public void start(Stage primaryStage){
        Survey model = new Survey();
        model.setEntry("other", 0);
        model.setEntry("Text",1);
        model.setEntry("Code",19);
        model.setEntry(";)",80);
        PieView view = new PieView();

        PiePresenter presenter = new PiePresenter(model, view);

        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }
}

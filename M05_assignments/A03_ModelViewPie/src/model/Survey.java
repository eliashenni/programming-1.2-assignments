package model;


import java.util.HashMap;
import java.util.Map;

public class Survey {
    private final Map<String, Integer> map = new HashMap<>();

    public Survey() {
    }

    public void setEntry(String answer, int percentage) {
        map.put(answer, percentage);
    }

    public Map<String, Integer> getMap() {
        return map;
    }
}

package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import model.Survey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PiePresenter {
    Survey model;
    PieView view;

    public PiePresenter(Survey model, PieView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
        updateView();
    }

    public void addEventHandlers() {
        this.view.getTextField().setOnAction(event -> {setValuesToPie();});
    }

    public void updateView() {
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        List<String> answers = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : this.model.getMap().entrySet()) {
            pieChartData.add(new PieChart.Data(entry.getKey(), entry.getValue()));
            answers.add(entry.getKey());
        }

        this.view.getPieChart().setData(pieChartData);

        this.view.getComboBox().getItems().clear();
        this.view.getComboBox().getItems().addAll(answers);
    }

    public void setValuesToPie() {
        String comboBox = this.view.getComboBox().getValue();
        String textField = this.view.getTextField().getText();

        Integer percentage = Integer.valueOf(textField);

        this.model.setEntry(comboBox, percentage);
        updateView();
    }

}

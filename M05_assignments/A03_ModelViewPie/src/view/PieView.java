package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class PieView extends BorderPane {
    private PieChart pieChart;
    private Label label;
    private ComboBox<String> comboBox;
    private TextField textField;

    public PieView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        this.pieChart = new PieChart();
        this.label = new Label("Change a slice:");
        this.comboBox = new ComboBox<>();
        this.textField = new TextField();
    }

    private void layoutNodes() {
        this.pieChart.setTitle("Where do I type semicolons?");
        this.setCenter(this.pieChart);

        HBox hBox = new HBox();
        hBox.getChildren().add(label);
        hBox.getChildren().add(comboBox);
        hBox.getChildren().add(textField);

        hBox.setSpacing(30);
        hBox.setPadding(new Insets(15, 50, 15, 50));

        this.setBottom(hBox);
    }

    PieChart getPieChart() {
        return pieChart;
    }

    Label getLabel() {
        return label;
    }

    ComboBox<String> getComboBox() {
        return comboBox;
    }

    TextField getTextField() {
        return textField;
    }
}

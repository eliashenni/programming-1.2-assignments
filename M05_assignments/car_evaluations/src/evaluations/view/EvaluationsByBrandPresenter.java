package evaluations.view;

import evaluations.model.Evaluation;
import evaluations.model.Evaluations;
import javafx.scene.chart.XYChart;

import java.util.List;
import java.util.Map;

public class EvaluationsByBrandPresenter {
    private final Evaluations model;
    private final EvaluationsView view;
    private static final String USER_RATING_LABEL = "User Rating";
    private static final String MILEAGE_LABEL = "Mileage";
    private static final String SAFETY_LABEL = "Safety";

    public EvaluationsByBrandPresenter(Evaluations model, EvaluationsView view) {
        this.model = model;
        this.view = view;

        this.addEventHandlers();
        this.updateView();
    }

    private void addEventHandlers() {
    }

    private void updateView() {
        this.view.getBarChart().getXAxis().setLabel("Cars");
        XYChart.Series<String, Number> ratingSeries = makeSeries(USER_RATING_LABEL);
        XYChart.Series<String, Number> mileageSeries = makeSeries(MILEAGE_LABEL);
        XYChart.Series<String, Number> safetySeries = makeSeries(SAFETY_LABEL);
        for (Map.Entry<String, Evaluation> evaluation : model.getResults().entrySet()) {
            ratingSeries.getData().add(new XYChart.Data<String, Number>(
                    evaluation.getKey(),
                    evaluation.getValue().getUserRating())
            );
            mileageSeries.getData().add(new XYChart.Data<String, Number>(
                    evaluation.getKey(),
                    evaluation.getValue().getMileage())
            );
            safetySeries.getData().add(new XYChart.Data<String, Number>(
                    evaluation.getKey(),
                    evaluation.getValue().getSafety())
            );
        }
        view.getBarChart().getData().addAll(List.of(ratingSeries,mileageSeries,safetySeries));
    }

    private XYChart.Series<String, Number> makeSeries(String userRatingLabel) {
        XYChart.Series<String, Number> ratingSeries = new XYChart.Series<>();
        ratingSeries.setName(userRatingLabel);
        return ratingSeries;
    }
}

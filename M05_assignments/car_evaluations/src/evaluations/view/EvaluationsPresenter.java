package evaluations.view;

import evaluations.model.Evaluation;
import evaluations.model.Evaluations;
import javafx.scene.chart.Axis;
import javafx.scene.chart.XYChart;

import java.util.Map;

public class EvaluationsPresenter {
	private final Evaluations model;
	private final EvaluationsView view;
	private static final String USER_RATING_LABEL = "User Rating";
	private static final String MILEAGE_LABEL = "Mileage";
	private static final String SAFETY_LABEL = "Safety";

	public EvaluationsPresenter(Evaluations model, EvaluationsView view) {
		this.model = model;
		this.view = view;

		this.addEventHandlers();
		this.updateView();
	}

	private void addEventHandlers() {
	}

	private void updateView() {
		this.view.getBarChart().getXAxis().setLabel("Criteria");
		for (Map.Entry<String, Evaluation> entry : this.model.getResults().entrySet()) {
			XYChart.Series<String, Number> series = new XYChart.Series<>();

			series.setName(entry.getKey());
			series.getData().add(new XYChart.Data<String, Number>(
					USER_RATING_LABEL,
					entry.getValue().getUserRating())
			);
			series.getData().add(new XYChart.Data<String, Number>(
					MILEAGE_LABEL,
					entry.getValue().getMileage())
			);
			series.getData().add(new XYChart.Data<String, Number>(
					SAFETY_LABEL,
					entry.getValue().getSafety())
			);

			this.view.getBarChart().getData().add(series);

		}
	}
}


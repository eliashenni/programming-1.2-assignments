package evaluations.view;

import javafx.scene.chart.*;
import javafx.scene.layout.BorderPane;

public class EvaluationsView extends BorderPane {
    private BarChart<String, Number> barChart;

    public EvaluationsView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        barChart = new BarChart<>(new CategoryAxis(), new NumberAxis());
    }

    private void layoutNodes() {
        this.setCenter(barChart);
    }

    BarChart<String, Number> getBarChart() {
        return barChart;
    }
}

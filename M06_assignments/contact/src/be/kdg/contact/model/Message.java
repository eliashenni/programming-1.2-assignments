package be.kdg.contact.model;

public class Message {
    private final String lastName;
    private final String firstName;
    private final String email;
    private final String messageBody;

    public Message(String lastName, String firstName, String email, String messageBody) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.messageBody = messageBody;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }

    public String getMessageBody() {
        return messageBody;
    }
}

package be.kdg.contact.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Messenger {
    public void send(Message message) throws IOException {
        save(message);
    }

    private void save(Message message) throws IOException {
        FileWriter fileWriter = new FileWriter("LastName_FirstName.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        PrintWriter printWriter = new PrintWriter(bufferedWriter);

        try {
            printWriter.printf("Last name:   %s\n", message.getLastName());
            printWriter.printf("First name:  %s\n", message.getFirstName());
            printWriter.printf("Email:       %s\n", message.getEmail());
            printWriter.printf("Message Body:\n%s", message.getMessageBody());

        } finally {
            printWriter.close();
        }
    }
}

package be.kdg.contact.view;

import be.kdg.contact.model.Message;
import be.kdg.contact.model.Messenger;
import javafx.scene.control.Alert;

import java.io.IOException;

public class ContactPresenter {
    private final Messenger model;
    private final ContactView view;

    public ContactPresenter(Messenger model, ContactView view) {
        this.model = model;
        this.view = view;

        addEventHandlers();
    }

    private void addEventHandlers() {
        this.view.getSubmitButton().setOnAction(event -> {
            final String lastName = view.getLastNameInput().getText();
            final String firstName = view.getFirstNameInput().getText();
            final String email = view.getEmailInput().getText();
            final String messageBody = view.getMessageBodyInput().getText();

            try {
                Message message = new Message(lastName, firstName, email, messageBody);
                model.send(message);

                Alert sent = new Alert(Alert.AlertType.INFORMATION);
                sent.setHeaderText("Message Sent");
                sent.setContentText(String.format("To: %s %s", lastName, firstName));
                sent.showAndWait();

                updateView();
            } catch (IOException e) {
                final Alert sendAlert = new Alert(Alert.AlertType.ERROR);
                sendAlert.setHeaderText("Error sending message");
                sendAlert.setContentText(e.toString());
                sendAlert.showAndWait();
            }
            updateView();
        });
    }

    private void updateView() {
        view.clearInput();
    }
}

package be.kdg.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        Path dir = Paths.get("module6");
        Path file = dir.resolve("my_file.txt");

        if (!Files.exists(dir)) {
            Files.createDirectory(dir);
        }

        if (!Files.exists(file)) {
            Files.createFile(file);
        }

        Files.write(file, "Elias Henni".getBytes());

        Map<String, Object> attributes = Files.readAttributes(file, "*");
        for (Map.Entry<String, Object> entry : attributes.entrySet()) {
            System.out.printf("%-20s: %s%n", entry.getKey(), entry.getValue());
        }

        if (!Files.exists(dir.resolve("my_files_copy.txt"))) {
            Files.copy(file, dir.resolve("my_files_copy.txt"));
        }
    }
}

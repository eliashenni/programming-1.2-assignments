package be.kdg.message.view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.*;
import java.util.Base64;

public class MessagePresenter {
    private final MessageView view;

    public MessagePresenter(MessageView view) {
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        final EventHandler<ActionEvent> eventHandler = event -> updateView();
        view.getForegroundPicker().setOnAction(eventHandler);
        view.getBackgroundPicker().setOnAction(eventHandler);

        view.getMessageField().textProperty().addListener((observable, oldValue, newValue) -> updateView());

        view.getLoadMenuItem().setOnAction(event -> {
            FileChooser open = new FileChooser();
            open.setTitle("Open");
            File file = open.showOpenDialog(this.view.getScene().getWindow());

            if (file == null) {
                return;
            }

            try {
                DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));

                short foregroundRed = dataInputStream.readShort();
                short foregroundGreen = dataInputStream.readShort();
                short foregroundBlue = dataInputStream.readShort();
                short backgroundRed = dataInputStream.readShort();
                short backgroundGreen = dataInputStream.readShort();
                short backgroundBlue = dataInputStream.readShort();

                char[] buffer = new char[50];
                StringBuilder sb = new StringBuilder();
                int length = 0;
                int numberOfBytes;
                while((numberOfBytes = dataInputStream.read())!= -1) {
                    buffer[length++] = (char) numberOfBytes;
                }
                sb.append(new String(buffer, 0, length));
                String decodedContent = new
                        String(Base64.getDecoder().decode(sb.toString()));

                this.view.getMessageField().setText(decodedContent);
                this.view.getForegroundPicker().setValue(Color.rgb(foregroundRed, foregroundGreen, foregroundBlue));
                this.view.getBackgroundPicker().setValue(Color.rgb(backgroundRed, backgroundGreen, backgroundBlue));

            } catch (IOException e) {
                e.printStackTrace(); 
            }

            updateView();
        });

        view.getSaveMenuItem().setOnAction(event -> {
            FileChooser save = new FileChooser();
            save.setTitle("Save");
            File file = save.showSaveDialog(this.view.getScene().getWindow());

            if (file == null) {
                return;
            }

            try {
                DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));

                String text = this.view.getMessageField().getText();
                Color foreground = this.view.getForegroundPicker().getValue();
                Color background = this.view.getBackgroundPicker().getValue();

                short foregroundRed = (short) (foreground.getRed() * 255);
                short foregroundGreen = (short) (foreground.getGreen() * 255);
                short foregroundBlue = (short) (foreground.getBlue() * 255);
                short backgroundRed = (short) (background.getRed() * 255);
                short backgroundGreen = (short) (background.getGreen() * 255);
                short backgroundBlue = (short) (background.getBlue() * 255);

                dataOutputStream.writeShort(foregroundRed);
                dataOutputStream.writeShort(foregroundGreen);
                dataOutputStream.writeShort(foregroundBlue);
                dataOutputStream.writeShort(backgroundRed);
                dataOutputStream.writeShort(backgroundGreen);
                dataOutputStream.writeShort(backgroundBlue);

                dataOutputStream.write(Base64.getEncoder().encode(text.getBytes()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            updateView();
        });
    }

    private void updateView() {
        final String message = view.getMessageField().getText();
        final Color foreground = view.getForegroundPicker().getValue();
        final Color background = view.getBackgroundPicker().getValue();
        view.showMessage(message, foreground, background);
    }
}

package be.kdg.students.view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class StudentView extends GridPane {
    private Label lastName;
    private Label firstName;
    private Label number;
    private Label group;
    private TextField firstNameInput;
    private TextField lastNameInput;
    private TextField numberInput;
    private TextField groupInput;
    private Button next;
    private Button previous;

    public StudentView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        lastName = new Label("Last name");
        firstName = new Label("First name");
        number = new Label("Number");
        group = new Label("Group");
        firstNameInput = new TextField();
        lastNameInput = new TextField();
        numberInput = new TextField();
        groupInput = new TextField();
        next = new Button("Next");
        previous = new Button("Previous");

        groupInput.setEditable(false);
        firstNameInput.setEditable(false);
        numberInput.setEditable(false);
        lastNameInput.setEditable(false);
    }

    private void layoutNodes() {
        setHgap(10);
        setVgap(10);
        setPadding(new Insets(10));
        add(lastName, 0, 0);
        add(firstNameInput, 1, 0);
        add(firstName, 0, 1);
        add(lastNameInput, 1, 1);
        add(number, 0, 2);
        add(numberInput, 1, 2);
        add(group, 0, 3);
        add(groupInput, 1, 3);
        add(previous, 0, 4);
        add(next, 1, 4);
    }

    Button getPrevious() {
        return previous;
    }

    Button getNext() {
        return next;
    }

    TextField getFirstNameInput() {
        return firstNameInput;
    }

    TextField getLastNameInput() {
        return lastNameInput;
    }

    TextField getNumberInput() {
        return numberInput;
    }

    TextField getGroupInput() {
        return groupInput;
    }
}

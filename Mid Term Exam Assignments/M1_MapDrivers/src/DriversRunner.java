import be.kdg.drivers.Driver;
import be.kdg.drivers.Drivers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DriversRunner {

    private static String[][] champions = {
            {"Germany", "Michael Schumacher"},
            {"Argentina", "Juan Manuel Fangio"},
            {"France", "Alain Prost"},
            {"German", "Sebastian Vettel"},
            {"Australia", "Jack Brabham"},
            {"United Kingdom", "Jackie Stewart"},
            {"Austria", "Niki Lauda"},
            {"Brasil", "Nelson Piquet"},
            {"Brasil", "Ayrton Senna"},
            {"United Kingdom","Lewis Hamilton"}
    };

    private static Integer[][] years = {
            {1994, 1995, 2000, 2001, 2002, 2003, 2004},
            {1951, 1954, 1955, 1956, 1957},
            {1985, 1986, 1989, 1993},
            {2010, 2011, 2012, 2013},
            {1959, 1960, 1966},
            {1969, 1971, 1973},
            {1975, 1977, 1984},
            {1981, 1983, 1987},
            {1988, 1990, 1991},
            {2008,2014,2015,2017,2018,2019,2020}
    };

    public static void main(String[] args) {
        Drivers drivers = new Drivers(champions, years);
//        System.out.println(drivers);

        System.out.println(drivers.getDriverByName("Jackie Stewart"));

        System.out.println("2000 champion: " + drivers.getDriverByYear(2000));
        System.out.println("2005 champion: " + drivers.getDriverByYear(2005));
        System.out.println();

        List<Driver> driversSort = new ArrayList<>(drivers.getDrivers());
        Collections.sort(driversSort);
        Collections.reverse(driversSort);

        System.out.println("DRIVERS BY TITLES");
        for (Driver d : driversSort) {
            System.out.print(d);
        }
    }
}
package be.kdg.drivers;

import java.util.TreeSet;

public class Driver implements Comparable<Driver>{
    private String name;
    private String nationality;
    TreeSet<Integer> worldChampYears;

    public Driver(String name, String nationality, TreeSet<Integer> worldChampYears) {
        this.name = name;
        this.nationality = nationality;
        this.worldChampYears = worldChampYears;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("%s (%s) %d times champion %s\n", name, nationality,worldChampYears.size(), worldChampYears);
    }

    @Override
    public int compareTo(Driver o) {
        int worldChampYearsDiff = this.worldChampYears.size() - o.worldChampYears.size();
        return (worldChampYearsDiff != 0) ? worldChampYearsDiff : Integer.compare(o.worldChampYears.last(), this.worldChampYears.last());
    }
}

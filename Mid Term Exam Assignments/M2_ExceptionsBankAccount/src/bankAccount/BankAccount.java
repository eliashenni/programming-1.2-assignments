package bankAccount;

public class BankAccount {
    private final String account;

    public BankAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < account.length(); i++) {
            sb.append(account.charAt(i));
            if (i == 2 || i == 9) {
                sb.append("-");
            }
        }
        return sb.toString();
    }
}
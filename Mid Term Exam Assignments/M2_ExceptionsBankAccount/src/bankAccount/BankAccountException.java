package bankAccount;

public class BankAccountException extends Exception{
    public BankAccountException() {
    }

    public BankAccountException(String message) {
        super(message);
    }

    public BankAccountException(Throwable cause) {
        super(cause);
    }

    public BankAccountException(String message, Throwable cause) {
        super(message, cause);
    }
}

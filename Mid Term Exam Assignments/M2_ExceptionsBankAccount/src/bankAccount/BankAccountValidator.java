package bankAccount;

public class BankAccountValidator {
    private static final int LENGTH = 12;

    public static void validateAccount(String account) throws BankAccountException {
        if (account.length() != LENGTH) {
            throw new BankAccountException("BankAccount must have 12 digits");
        } else if (!account.matches("[0-9]+")) {
            throw new BankAccountException("String must be numeric");
        } else if (!isValidNumber(account)) {
            throw new BankAccountException("Wrong account number");
        }
    }

    public static boolean isValidNumber(String account) {
        long remainder = Long.parseLong(account.substring(0,10)) % 97;
        int control = Integer.parseInt(account.substring(10,12));

        if (remainder == 0) {
            return control == 97;
        } else {
            return remainder == control;
        }
    }
}

package retirementCalculator;

import retirementCalculator.model.Retirement;
import retirementCalculator.view.RetirementPresenter;
import retirementCalculator.view.RetirementView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class RCMain extends Application {
    @Override
    public void start(Stage primaryStage) {
        Retirement model = new Retirement();
        RetirementView view = new RetirementView();
        new RetirementPresenter(model, view);
        primaryStage.setScene(new Scene(view));
        primaryStage.setTitle("Retirement Calculator");
        primaryStage.show();

    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}


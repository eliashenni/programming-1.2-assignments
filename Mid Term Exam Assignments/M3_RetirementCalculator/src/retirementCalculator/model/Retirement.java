package retirementCalculator.model;

public class Retirement {
    private int birthYear;
    private static final int RETIREMENT_AGE = 67;


    // private attributes

    public Retirement() {
        // Constructor
    }

    // methods with business logic
    public int getRetirementYear() {
        return birthYear + RETIREMENT_AGE;
    }

    // needed getters and setters

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
}


package retirementCalculator.view;

import retirementCalculator.model.Retirement;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RetirementPresenter {
    private Retirement model;
    private RetirementView view;

    public RetirementPresenter(
            Retirement model, RetirementView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getBirthYearButton().setOnAction(new eventHandler());
    }

    private void updateView() {
        view.getShowRetirementYear().setText(String.format("%d", model.getRetirementYear()));
    }

    class eventHandler implements EventHandler<ActionEvent> {
        @Override
        public void handle(ActionEvent event) {
            try {
                model.setBirthYear(Integer.parseInt(view.getInput().getText()));
                updateView();
            } catch (NumberFormatException e ) {
                view.getShowRetirementYear().setText("Invalid input!");
            }
        }
    }

}
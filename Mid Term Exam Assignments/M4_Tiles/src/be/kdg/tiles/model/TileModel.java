package be.kdg.tiles.model;

public class TileModel {
    public static final int COLUMNS = 9;
    public static final int ROWS = 9;

    private int selectedTileColumn;
    private int selectedTileRow;

    public TileModel() {
        this.selectedTileColumn = -1;
        this.selectedTileRow = -1;
    }

    public void setSelectedTile(int column, int row) {
        if (this.selectedTileColumn == column && this.selectedTileRow == row) {
            this.selectedTileColumn = -1;
            this.selectedTileRow = -1;
        }
        else {
            this.selectedTileColumn = column;
            this.selectedTileRow = row;
        }
    }

    public int getSelectedTileColumn() {
        return this.selectedTileColumn;
    }

    public int getSelectedTileRow() {
        return this.selectedTileRow;
    }
}

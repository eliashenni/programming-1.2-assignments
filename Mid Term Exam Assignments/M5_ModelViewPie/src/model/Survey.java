package model;


import java.util.HashMap;
import java.util.Map;

public class Survey {
    private final Map<String, Integer> chartData = new HashMap<>();

    public Survey() {
    }

    public void setEntry(String answer, int percentage) {
        chartData.put(answer, percentage);
    }

    public Map<String, Integer> getChartData() {
        return chartData;
    }
}
